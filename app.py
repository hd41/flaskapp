# Importing flask module in the project is mandatory
# An object of Flask class is our WSGI application.
from flask import Flask, render_template, request
from flask_basicauth import BasicAuth
from flask_mail import Mail, Message
from random import randint
import json

# Flask constructor takes the name of
# current module (__name__) as argument.
app = Flask(__name__)

app.config.update(
    MAIL_SERVER='smtp@gmail.com',
    MAIL_PORT=587,
    MAIL_USE_SSL=True,
    MAIL_USERNAME = 'jarvis1247@gmail.com',
    MAIL_PASSWORD = 'Shreeram@610'
)
mail = Mail(app)

app.config['BASIC_AUTH_USERNAME'] = 'jarvis'
app.config['BASIC_AUTH_PASSWORD'] = 'matrix'

basic_auth = BasicAuth(app)

@app.route('/')
def hello_world():
    return render_template('index.html')

def mail_to_self(name, feedback):
    try:
        msg = Message(subject="Feedback from "+str(name.capitalize()),
                      body=feedback,
                      sender="jarvis1247@gmail.com",
                      recipients=["dhanwant.himanshu41@gmail.com", "jarvis1247@gmail.com"])
        mail.send(msg)
        return 1
    except Exception as ex:
        print(ex)
        return 0

@app.route('/feedback', methods=['GET', 'POST'])
def feedback():
    if request.method == 'GET':
        return render_template('feedback.html')
    elif request.method == 'POST':
        name = request.values.get('name') # Your form's
        feedback = request.values.get('feedback')
        print(name, feedback)
        if len(name.strip()) == 0:
            name='Anonymous'
        if (mail_to_self(name, feedback)):
            return '<div>Your feedback has been submitted. Click <a href="/feedback">here</a> to submit other.</div>'
        else:
            return "Not able to send feedback to Himanshu. Please retry after sometime."

@app.route('/info', methods=['POST'])
@basic_auth.required
def secret_view():
    data = {
        'result': 'You can have '+str(randint(1,4))+' child(ren).'
    }
    return json.dumps(data)

# main driver function
if __name__ == '__main__':
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(debug=True)
